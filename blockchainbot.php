<?php
# Requires shuber's cURL wrapper for PHP
# https://github.com/shuber/curl
# Also requires Pawl
# https://github.com/ratchetphp/Pawl
# Runnning this script as a service in the background
# https://stackoverflow.com/questions/2036654/run-php-script-as-daemon-process
require __DIR__ . '/vendor/autoload.php';
require_once 'curl.php';
set_time_limit(0);

/***** Constants *****/
define("MY_ADDRESS", ""); # insert address to be listened here
define("TEST_ADDRESS", "17A16QmavnUfCW11DAApiJxp7ARnxN5pGX"); # dummy addr. TXs every 3 minutes
define("TEST_ADDRESS2", "3GsTsD3BnZhCroj7VecnjFjEMa8BBCAc6o"); # Alejandro
/*********************/

/***** Variables *****/
$curl 		= "";
$chGET 		= false;
$response = false;
$chPOST 	= false;
$transfer = "";
$options 	= array();
$chGET 		= false;
$msg 			= false;
$sentBy 	= "";
$receiver = "";
$value 		= 0;
$time 		= "";
$data 		= "";
/*********************/

/***** Processes *****/
#getAddressData("17A16QmavnUfCW11DAApiJxp7ARnxN5pGX");
listenToTransactions();
/*********************/

/**** General Fns ****/
function satoshi_to_BTC($val)
{
	return $val / 100000000;
}
/*********************/

/*** Key Functions ***/
function listenToTransactions()
{
	\Ratchet\Client\connect('ws://ws.blockchain.info/inv')->then(function ($conn) {
		echo "<script>alert('Now listening...')</script>";
		# $conn->send('{"op":"unconfirmed_sub"}');
		 $conn->send('{"op":"addr_sub", "addr": "' . TEST_ADDRESS . '"}');
		$conn->on('message', function ($tx) use ($conn) {read_tx($tx);});
		$conn->on('close', function ($code = null, $reason = null) {
			echo "<p style='background: coral; color: white'> Connection closed - Restarting...</p>";
			listenToTransactions();
		});
	}, function ($e) {echo "<p style='background: coral; color: white'>Could not connect: ".$e->getMessage()."</p>";});
}

function read_tx($tx) {
	# Txs can be confirmed through https://chain.so/api/v2/is_tx_confirmed/BTC/$hash
	$tx = $tx->__toString();
	$tx = json_decode($tx);
	foreach ($tx->x->out as $i => $item) {
		if($item->addr === TEST_ADDRESS) {
			$sentBy = $tx->x->inputs[0]->prev_out->addr;
			$receiver = $item->addr;
			$value = satoshi_to_BTC($item->value);
			$hash  = $tx->x->hash;
			$time  = date("h:m:s a, m/d/Y", $tx->x->time);
			write_tx($sentBy, $receiver, $value, $hash, $time);
		}
	}
}

function write_tx($sentBy, $receiver, $value, $hash, $time) {
	$chGET = curl_init();
	$options = array(
		CURLOPT_URL => "http://localhost/Blockchainbot/scripts/wallet_system.php?fromAddress=".$sentBy,
		CURLOPT_RETURNTRANSFER => true
	);
	curl_setopt_array($chGET, $options);
	$userfrom = curl_exec($chGET);

	if ($userfrom) {
		echo "<div><span style='background: green; color: white; padding: 2px 4px 2px 4px'><b>New transaction!</b></span>";
		echo "@ $time";
		echo "<br>\--> Sent by: $sentBy";
		echo "<br>\--> Sent to: $receiver";
		echo "<br>\--> Amount: $value BTC";
		echo "<br>\--> Tx Hash: {$hash}";
		echo "<br>\--> Made by user: {$userfrom}</div><br>";
		$data = array(
			"key"=> "02236811",
			"userfrom"=> $userfrom,
			"senderAddr"=> $sentBy,
			"amount"=> $value,
			"isDeposit"=> true,
			"hash"=> $hash
		);
		$chPOST = curl_init();
		$options = array(
			CURLOPT_URL => "http://localhost/Blockchainbot/scripts/transfer_system.php",
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($data),
			CURLOPT_RETURNTRANSFER => true);
		curl_setopt_array($chPOST, $options);
		$transfer = curl_exec($chPOST);
		if(!$transfer) {
			echo '[2]Request Failed: '. curl_error($chPOST);
			echo '[2]Request Failed: '. curl_error($transfer);
		}
		else
			echo $transfer;
		curl_close($chPOST);
	} else {
		echo "[1]Request Failed ('".curl_errno($chGET)."'): ".curl_error($chGET);
	}
	curl_close($chGET);
	/*********************/
}

function getAddressData($addr) {
	# This fn receives a $wallet and starts listening to it
	$curl = new Curl;
	echo "<p><b>Consulting</b> Transactions for Bitcoin address '{$addr}'...</p>";

	$response = $curl->get("https://blockchain.info/es/rawaddr/$addr");
	echo "https://blockchain.info/es/rawaddr/$addr";
	$response = json_decode($response->body);
	$sentBy = $response->address;
	echo "<br>Address: " . $response->address;
	echo "<br>Current balance: ".satoshi_to_BTC($response->final_balance)." BTC";
	echo "<br>Number of Tx (sent and received): {$response->n_tx}";
	echo "<br><br><b>Received transactions:</b><br>";
	foreach ($response->txs as $i => $tx) {
		foreach ($tx->out as $j => $out) {
			if ($out->addr === $addr) {
				echo "| Tx #$j at " . date("h:m:s a, m/d/Y", $tx->time);
				#echo "<br>\--> Sent by '$tx->inputs[0]->prev_out->addr'";
				echo "<br>\--> Amount: " . satoshi_to_BTC($out->value) . " BTC";
				echo "<br>\--> Tx Hash: " . $tx->hash . "<br><br>";
			}
		}
	}
	echo "<br><b>Real-time transactions:</b>";
	#listenToTransactions();
}
?>