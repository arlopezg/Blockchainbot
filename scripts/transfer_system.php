<?php
	require_once '../curl.php';
	include("config.php");
	/****** Variables *******/
  // Would recommend it not being too short
  $key 				= "02236811";
	$userkey 		= "";
	$userfrom 	= "";
	$amount 		= 0;
	$userto 		= "";
	$msgGame 		= "";
	$senderAddr = "";
	$chPOST 		= "";
	$options 		= array();
	/************************/

	/****** Processes *******/
	/************************/
	
	/****** Specific Fns ****/
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset($_POST['key']))
			$userkey = $_POST['key'];
		else
			$msgGame = "NO KEY";
		// if keys don't match, stop
		if ($key !== $userkey)
			$error = "Key invalid!";
		else if (isset($_POST['isDeposit'])) {
			$userfrom 	= $_POST['userfrom'];
			$newBalance = (isset($_POST['amount'])) ? $_POST['amount'] : 0;
			$newBalance += getUserBalance($db, $userfrom);
			setUserBalance($db, $userfrom, $_POST['amount'], $newBalance);
			writeTx($db, $_POST['hash'], $_POST['amount']);
		} else {
			// get POST data
			$userfrom = mysqli_real_escape_string($db, $_POST['userfrom']);
			$amount		= mysqli_real_escape_string($db, $_POST['amount']); 
			$userto 	= mysqli_real_escape_string($db, $_POST['userto']);

			// checks if 'userfrom' exists
			$sql 		= "SELECT id FROM users WHERE username = '$userfrom'";
			$result = mysqli_query($db,$sql);		
			$count1 = mysqli_num_rows($result);
			
			// checks if 'userto' exists
			$sql 		= "SELECT id FROM users WHERE username = '$userto'";
			$result = mysqli_query($db,$sql);		
			$count2 = mysqli_num_rows($result);
				
			// if both users exist, continue
			if ($count1 == 1 AND $count2 == 1) {
				// get's coins amount from money table if user exists
				$sql 		= "SELECT coins FROM money WHERE username = '$userfrom'";
				$result	= mysqli_query($db,$sql);		
				$count 	= mysqli_num_rows($result);
				$coins 	= mysqli_fetch_assoc($result)['coins'];
				
				// if username has coins then proceed to take amount
				if ($count == 1) { 
					$enoughCoins = true;						
					$calc = $coins - $amount;
					if ($calc >= 0)
						$coins -= $amount;
					else
						$enoughCoins = false;

					if ($enoughCoins) {
						if ($userto === "Deposit"){
							$senderAddr = mysqli_real_escape_string($db, $_POST['senderAddr']);
							$data = array(
								"key"=> "02236811",
								"senderAddr"=> $senderAddr,
								"amount"=> $amount,
								"outgoing"=> true
							);
							$chPOST = curl_init();
							$options = array(
								CURLOPT_URL => "http://localhost/Blockchainbot/scripts/outgoing_transfer_system.php",
								CURLOPT_POST => true,
								CURLOPT_POSTFIELDS => http_build_query($data),
								CURLOPT_RETURNTRANSFER => true);
							curl_setopt_array($chPOST, $options);
							$outgoing = curl_exec($chPOST);

							if($outgoing) {
								var_dump($outgoing);
							}
							else
								# outgoing transfer couldn't be processed
								echo curl_error($chPOST);
						} else {
						$sql = "UPDATE money SET coins = '$coins' WHERE username = '$userfrom'";
						$result = mysqli_query($db,$sql);
						
						if ($result) {
							$sql = "SELECT coins FROM money WHERE username = '$userto'";
							$result = mysqli_query($db,$sql);		
							$count = mysqli_num_rows($result);
							$coins = mysqli_fetch_assoc($result)['coins'];
							
							if ($count == 1) {
								$coins += $amount;
								$sql = "UPDATE money SET coins='$coins' WHERE username = '$userto'";
								$result = mysqli_query($db,$sql);
								if ($result) {
									$msgGame = "SUCCESS";
									LogTransaction($db,$userfrom, $amount, $userto);
									LogTransactioncustom($db, $userfrom, $amount, $userto);
								}
								else{
									$msgGame = "FAIL1";
									$error = "Error in giving coins!! #1";
								}
							}
							else if ($count == 0) {
								$sql = "INSERT INTO money (username, coins) VALUES ('$userto', '$amount')";
								$result = mysqli_query($db,$sql);	
								
								if ($result) {
									$msgGame = "SUCCESS";
									LogTransaction($db,$userfrom, $amount, $userto);
									LogTransactioncustom($db, $userfrom, $amount, $userto);
								}
								else {
									$msgGame = "FAIL2";
									$error = "Error in giving/taking coins!! #2";
								}
							}
						}
						}
					}
					else{
						$msgGame = "FAIL3";
						$error = "Error, user not enough coins!! #3";
					}
				}
			}
			else{
				$msgGame = "FAIL4";
				$error = "Error, user not excisting in money table!! #3";
			}
		}

    if (isset($_POST['game'])) {
		echo $msgGame;	
		return;			
    }
	}
	/************************/

	/***** General Fns ******/
	function getUserBalance($_db, $_user) {
		$result = mysqli_query($_db, "SELECT coins FROM money WHERE username = '$_user'");
		$count = mysqli_num_rows($result);
		return mysqli_fetch_assoc($result)["coins"];
	}

	function setUserBalance($_db, $_userto, $_amount, $_balance) {
		$result = mysqli_query($_db, "UPDATE money SET coins='$_balance' WHERE username = '$_userto'");
		if ($result) {
			$msgGame = "SUCCESS";
			LogTransactioncustom($_db, $_userto, $_amount, "Deposit");
			echo "User '$_userto' successfully deposited $_amount	BTC to our accounts";
		}
		else {
			$msgGame = "FAIL1";
			$error = "Error in giving coins!! #1";
		}
	}

	function writeTx($_db, $_hash, $_amount) {
		mysqli_query($_db, "INSERT INTO txs (hash, amount) VALUES ('$_hash', $_amount)");
	}

	function LogTransaction($db, $_userfrom, $_amount, $_userto) {		
		$log = $_userfrom." paid ".$_amount." to ".$_userto.".";
		$sql = "INSERT INTO transactions (information) VALUES ('$log')";
		$result = mysqli_query($db,$sql);	
	}

	function LogTransactioncustom($db, $_userfrom, $_amount, $_userto) {		
		$log = $_userfrom." paid ".$_amount." to ".$_userto.".";
		$sql = "INSERT INTO transactionscustom (information) VALUES ('$log')";
		$result = mysqli_query($db,$sql);	
	}
	/************************/
?>
