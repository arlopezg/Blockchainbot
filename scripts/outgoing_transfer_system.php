<?php
	require '../curl.php';
	include("config.php");
	require '../api-v1-client-php/vendor/autoload.php';
	define("BLOCKCHAIN_IDENTIFIER", ""); # insert your Blockchain wallet identifier
	define("TEST_IDENTIFIER", "b71d8d58-f660-4cff-b7db-831ab74932b9"); # for test purposes
	/****** Variables *******/
  // Would recommend it not being too short
  $key 				= "02236811";
	$userkey 		= "";
	$userfrom 	= "";
	$amount 		= 0;
	$userto 		= "";
	$msgGame 		= "";
	$senderAddr = "";
	$chPOST 		= "";
	$options 		= array();
	global $Blockchain;
	# -----------------------#

	/************************/

	/****** Processes *******/
	$Blockchain = new \Blockchain\Blockchain();
	$Blockchain = setWalletProperties($Blockchain, TEST_IDENTIFIER, "842675319a!");
	/************************/
	
	/****** Specific Fns ****/
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$userfrom 	= isset($_POST["userfrom"]) ? $_POST["userfrom"] : "";
		$amount 		= isset($_POST["amount"]) ? $_POST["amount"] : "";
		$userto 		= isset($_POST["userto"]) ? $_POST["userto"] : "";
		$senderAddr = isset($_POST["senderAddr"]) ? $_POST["senderAddr"] : "";
		if($userfrom && $userto && $amount && $senderAddr)
			$response = newPayment($Blockchain, $userto, $amount, $userfrom, "0");
	}
	/************************/

	/***** General Fns ******/
	function getUserBalance ($_db, $_user) {
		$result = mysqli_query($_db, "SELECT coins FROM money WHERE username = '$_user'");
		$count = mysqli_num_rows($result);
		return mysqli_fetch_assoc($result)["coins"];
	}

	function setUserBalance ($_db, $_userto, $_amount, $_balance) {
		$result = mysqli_query($_db, "UPDATE money SET coins='$_balance' WHERE username = '$_userto'");
		if ($result) {
			$msgGame = "SUCCESS";
			LogTransactioncustom($_db, $_userto, $_amount, "Deposit");
			echo "User '$_userto' successfully deposited $_amount	BTC to our accounts";
		}
		else {
			$msgGame = "FAIL1";
			$error = "Error in giving coins!! #1";
		}
	}

	function setWalletProperties ($_Blockchain, $id, $pw) {
		$_Blockchain->setServiceUrl('http://localhost:3000');
		$_Blockchain->Wallet->credentials($id, $pw);
		return $_Blockchain;
	}


	function newPayment ($_Blockchain, $to_address, $amount, $from_address, $fee) {
		// $from_address and $fee are optional params
		// more info on https://github.com/blockchain/api-v1-client-php/blob/master/docs/wallet.md
		return $_Blockchain->Wallet->send($to_address, $amount, $from_address, $fee);
	}
	/************************/
?>
